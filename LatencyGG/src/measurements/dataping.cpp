//
// Created by tim on 16/08/2021.
//
#if defined(_MSC_VER)
#include "targetver.h"
#endif

#include <random>
#include <external/nlohmann/json.hpp>
#include <experimental/networking.hpp>

#include "latencygg/measurements/dataping.hpp"
#include "latencygg/common.hpp"

namespace latencygg {
    namespace dataping {
        using std::experimental::net::ip::address;

        DataPing::DataPing() {
            this->mState = eEmpty;
            this->mSeq = 0;
            this->mTimestampC0 = this->mTimestampS0 = this->mTimestampC1 = this->mTimestampS1 = this->mTimestampC2 = this->mTimestampS2 = this->mTimestampC3 = 0;
        }

        DataPing::DataPing(std::string aIdent, std::string aSourceIp, std::string aTargetIp, std::string aToken, int aDPVersion)
                : mTargetIp(aTargetIp), mSourceIp(aSourceIp), mIdent(aIdent), mToken(aToken), mDPVersion(aDPVersion) {
            std::default_random_engine generator;
            std::uniform_int_distribution<int> distribution(0, 255);
            this->mSeq = distribution(generator);
            this->mTimestampC0 = this->mTimestampS0 = this->mTimestampC1 = this->mTimestampS1 = this->mTimestampC2 = this->mTimestampS2 = this->mTimestampC3 = 0;
        }

        void DataPing::update(std::vector<uint8_t> aReply, size_t aReplyLength) {
            if (aReplyLength > 112) {
                this->mErrors++;
                return;
            }
            aReply.resize(aReplyLength);
            auto in_packet = DataPingResponseV2::deserialize(aReply);
            if (in_packet->mSeq != this->mSeq) {
                return;
            }

            switch (in_packet->mType) {
                default:
                case eSNull:
                    this->mErrors++;
                    break;
                case eSInit:
                    this->mState = eS0;
                    this->mTimestampS0 = in_packet->mTimestamp;
                    _memcpy_s(mSignatures[0], LATENCYGG_SIG_LEN, in_packet->mSignature, LATENCYGG_SIG_LEN);
                    break;
                case eSNext:
                    this->mState = eS1;
                    this->mTimestampS1 = in_packet->mTimestamp;
                    _memcpy_s(mSignatures[1], LATENCYGG_SIG_LEN, in_packet->mSignature, LATENCYGG_SIG_LEN);
                    break;
                case eSFinal:
                    this->mState = eComplete;
                    this->mTimestampS2 = in_packet->mTimestamp;
                    _memcpy_s(mSignatures[2], LATENCYGG_SIG_LEN, in_packet->mSignature, LATENCYGG_SIG_LEN);
                    this->mTimestampC3 = now();
                    break;
            }
        }

        std::vector<uint8_t> DataPing::generatePacket() {
            DataPingRequestContainer packet = DataPingRequestContainer(this->mDPVersion, this->mSeq, this->mIdent, this->mToken, now());
            switch (this->mState) {
                case eC0:
                    this->mTimestampC0 = packet.mTimestamp;
                    packet.setType(eCInit);
                    break;
                case eS0:
                case eC1:
                    this->mTimestampC1 = packet.mTimestamp;
                    packet.setType(eCSecond);
                    break;
                case eS1:
                case eC2:
                    this->mTimestampC2 = packet.mTimestamp;
                    packet.setType(eCFinal);
                    break;
                default:
                case eKilled:
                case eEmpty:
                case eComplete:
                    throw std::out_of_range("tried to generate packet from invalid mState");
            }
            return packet.serialize();
        }

        bool DataPing::isComplete() {
            return this->mState == eComplete;
        }

        bool DataPing::isDead() {
            return ((this->mErrors > 3) || (this->mState == eKilled));
        }

        bool DataPing::isEmpty() {
            return this->mState == eEmpty;
        }

        std::shared_ptr<Stats> DataPing::getStats() {
            std::shared_ptr<Stats> stats = std::make_shared<Stats>();
            std::vector<timestamp_millisec_t> rtts;
            timestamp_millisec_t diff;
            rtts.resize(5);
            size_t count = 0;
            switch (this->mState) {
                case eComplete:
                    diff = (this->mTimestampC3 - this->mTimestampC2);
                    _memcpy_s(rtts.data() + 3, sizeof(diff) * (size_t)5, &diff, sizeof(diff));
                    diff = (this->mTimestampS2 - this->mTimestampS1);
                    _memcpy_s(rtts.data() + 4, sizeof(diff) * (size_t)5, &diff, sizeof(diff));
                    count += (size_t)2;
                case eS1:
                case eC2:
                    diff = (this->mTimestampC2 - this->mTimestampC1);
                    _memcpy_s(rtts.data() + 1, sizeof(diff) * (size_t)5, &diff, sizeof(diff));
                    diff = (this->mTimestampS1 - this->mTimestampS0);
                    _memcpy_s(rtts.data() + 2, sizeof(diff) * (size_t)5, &diff, sizeof(diff));
                    count++;
                case eS0:
                case eC1:
                    diff = (this->mTimestampC1 - this->mTimestampC0);
                    _memcpy_s(rtts.data(), sizeof(diff) * (size_t)5, &diff, sizeof(diff));
                    count++;
                default:
                case eKilled:
                case eEmpty:
                case eC0:
                    break;
            }
            rtts.resize(count);
            timestamp_millisec_t max_elm = *max_element(rtts.begin(), rtts.end());
            if (max_elm > (std::numeric_limits<uint64_t>::max() / std::max(count, (size_t) 1))) {
                throw std::out_of_range("nonsensical timestamp found");
            }
            timestamp_millisec_t sum = std::accumulate(rtts.begin(), rtts.end(), (timestamp_millisec_t) 0);
            long double rtt = std::round(((long double) sum / std::fmax(count, 1)));

            auto square_diff = [rtt](long double a, uint64_t b) {
                return a + std::pow((long double) b - rtt, 2);
            };
            long double variance =
                    std::accumulate(rtts.begin(), rtts.end(), (long double) 0, square_diff) / std::fmax(count, 1);
            stats->mRtt = std::lround(rtt);
            stats->mStddev = std::lround(std::fmin(std::sqrt(variance), *max_element(rtts.begin(), rtts.end())));
            return stats;
        }

        std::string DataPing::asJson() {
            auto stats = this->getStats();
            nlohmann::json jdata;
            nlohmann::json jraw;
            jraw["timestamp_c0"] = this->mTimestampC0;
            jraw["timestamp_c1"] = this->mTimestampC1;
            jraw["timestamp_c2"] = this->mTimestampC2;
            jraw["timestamp_c3"] = this->mTimestampC3;
            jraw["timestamp_s0"] = this->mTimestampS0;
            jraw["timestamp_s1"] = this->mTimestampS1;
            jraw["timestamp_s2"] = this->mTimestampS2;
            jraw["signatures"] = this->mSignatures;
            jraw["errors"] = this->mErrors;
            jdata["type"] = "udp-data";
            jdata["version"] = this->mVersion;
            jdata["ip"] = this->mSourceIp;
            jdata["ident"] = this->mIdent;
            jdata["rtt"] = stats->mRtt;
            jdata["stddev"] = stats->mStddev;
            jdata["raw"] = jraw;
            return jdata.dump();
        }

        void DataPing::kill() {
            this->mState = eKilled;
            this->mTimestampC0 = 0;
            this->mTimestampS0 = 500;
            this->mTimestampC1 = 1000;
            this->mTimestampS1 = 1500;
            this->mTimestampC2 = 2000;
            this->mTimestampS2 = 2500;
            this->mTimestampC3 = 3000;
        }

        void DataPing::overwrite(DataPing &dataPing) {
            this->mTimestampC0 = dataPing.mTimestampC0;
            this->mTimestampC1 = dataPing.mTimestampC1;
            this->mTimestampC2 = dataPing.mTimestampC2;
            this->mTimestampC3 = dataPing.mTimestampC3;
            this->mTimestampS0 = dataPing.mTimestampS0;
            this->mTimestampS1 = dataPing.mTimestampS1;
            this->mTimestampS2 = dataPing.mTimestampS2;
            for (int i = 0; i < 3; i++) {
                _memcpy_s(this->mSignatures[i], LATENCYGG_SIG_LEN, dataPing.mSignatures[i], LATENCYGG_SIG_LEN);
            }
            this->mErrors = dataPing.mErrors;
            this->mVersion = dataPing.mVersion;
            this->mSourceIp = dataPing.mSourceIp;
            this->mIdent = dataPing.mIdent;
            this->mState = dataPing.mState;
            this->mSeq = dataPing.mSeq;
        }

    }
}
