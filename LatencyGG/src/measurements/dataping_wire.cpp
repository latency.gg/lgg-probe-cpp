//
// Created by tim on 16/08/2021.
//
#if defined(_MSC_VER)
#include "targetver.h"
#endif
#include <chrono>
#include <external/endian.h>

#include "latencygg/common.hpp"
#include "latencygg/measurements/dataping_wire.hpp"


namespace latencygg {
    timestamp_millisec_t now() {
        return std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::system_clock::now().time_since_epoch()).count();
    }

    namespace wire {
        std::vector<uint8_t> DataPingRequestV2::serialize() {
            std::vector<uint8_t> body;
            size_t i = body.size();
            body.resize(i + sizeof(this->mVersion));
            int32_t little_version = htole32(this->mVersion);
            _memcpy_s(body.data() + i, i + sizeof(this->mVersion), &little_version, sizeof(this->mVersion));
            i = body.size();
            body.resize(i + sizeof(this->mType));
            int32_t little_type = htole32((int32_t) this->mType);
            _memcpy_s(body.data() + i, i + sizeof(this->mType), &little_type, sizeof(this->mVersion));
            i = body.size();
            body.resize(i + sizeof(this->mSeq));
            uint16_t little_seq = htole16(this->mSeq);
            _memcpy_s(body.data() + i, i + sizeof(this->mSeq), &little_seq, sizeof(this->mSeq));
            i = body.size();
            body.resize(i + LATENCYGG_IDENT_LEN - (size_t)1);
            _memcpy_s(body.data() + i, i + LATENCYGG_IDENT_LEN - 1, &(this->mIdent), LATENCYGG_IDENT_LEN - 1);
            i = body.size();
            char pad[7] = "\x00\x00\x00\x00\x00\x00";
            body.resize(i + sizeof(pad) - 1);
            _memcpy_s(body.data() + i, i + sizeof(pad) - (size_t)1, pad, sizeof(pad) - 1);
            i = body.size();
            body.resize(i + sizeof(this->mTimestamp));
            uint64_t little_ts = htole64(this->mTimestamp);
            _memcpy_s(body.data() + i, i + sizeof(this->mTimestamp), &little_ts, sizeof(this->mTimestamp));
            return body;
        }

        std::vector<uint8_t> DataPingRequestV3::serialize() {
            std::vector<uint8_t> body;
            size_t i = body.size();
            body.resize(i + sizeof(this->mVersion));
            int32_t little_version = htole32(this->mVersion);
            _memcpy_s(body.data() + i, i + sizeof(this->mVersion), &little_version, sizeof(this->mVersion));
            i = body.size();
            body.resize(i + sizeof(this->mType));
            int32_t little_type = htole32((int32_t) this->mType);
            _memcpy_s(body.data() + i, i + sizeof(this->mType), &little_type, sizeof(this->mVersion));
            i = body.size();
            body.resize(i + sizeof(this->mSeq));
            uint16_t little_seq = htole16(this->mSeq);
            _memcpy_s(body.data() + i, i + sizeof(this->mSeq), &little_seq, sizeof(this->mSeq));
            i = body.size();
            const char pad1[7] = "\x00\x00\x00\x00\x00\x00";
            body.resize(i + sizeof(pad1) - (size_t)1);
            _memcpy_s(body.data() + i, i + sizeof(pad1) - 1, pad1, sizeof(pad1) - 1);
            i = body.size();
            body.resize(i + LATENCYGG_TOKEN_LEN - (size_t)1);
            _memcpy_s(body.data() + i, i + LATENCYGG_TOKEN_LEN - 1, &(this->mToken), LATENCYGG_TOKEN_LEN - 1);
            i = body.size();
            const char pad2[9] = "\x00\x00\x00\x00\x00\x00\x00\x00";
            body.resize(i + sizeof(pad2) - (size_t)1);
            _memcpy_s(body.data() + i, i + sizeof(pad2) - 1, pad2, sizeof(pad2) - 1);
            i = body.size();
            body.resize(i + sizeof(this->mTimestamp));
            uint64_t little_ts = htole64(this->mTimestamp);
            _memcpy_s(body.data() + i, i + sizeof(this->mTimestamp), &little_ts, sizeof(this->mTimestamp));
            return body;
        }

        std::shared_ptr<DataPingResponseV2> DataPingResponseV2::deserialize(std::vector<uint8_t> aData) {
            std::shared_ptr<DataPingResponseV2> packet = std::make_shared<DataPingResponseV2>();
            size_t i = aData.size() - sizeof(timestamp_millisec_t);
            uint64_t little_ts;
            _memcpy_s(&little_ts, sizeof(uint64_t), aData.data() + i, sizeof(timestamp_millisec_t));
            packet->mTimestamp = le64toh(little_ts);
            aData.resize(i);
            i -= (size_t)6; // remove pad
            aData.resize(i);
            i -= (LATENCYGG_SIG_LEN - 1);  // copy all bytes of sig but leaving space for null char at the end
            _memcpy_s(&(packet->mSignature), LATENCYGG_SIG_LEN, aData.data() + i, LATENCYGG_SIG_LEN - 1);
            aData.resize(i);
            i -= sizeof(uint16_t);
            uint16_t little_seq;
            _memcpy_s(&little_seq, sizeof(uint16_t), aData.data() + i, sizeof(uint16_t));
            packet->mSeq = le16toh(little_seq);
            aData.resize(i);
            i -= sizeof(data_ping_response_type_t);
            int32_t little_type;
            _memcpy_s(&little_type, sizeof(int32_t), aData.data() + i, sizeof(data_ping_response_type_t));
            packet->mType = (data_ping_response_type_t) le32toh(little_type);
            aData.resize(i);
            i -= sizeof(int32_t);
            int32_t little_version;
            _memcpy_s(&little_version, sizeof(int32_t), aData.data() + i, sizeof(int32_t));
            packet->mVersion = le32toh(little_version);
            aData.resize(i);
            return packet;
        }


        void DataPingRequestContainer::setType(data_ping_request_type_t aRequestType) {
            this->mType = aRequestType;
        }

        std::vector<uint8_t> DataPingRequestContainer::serialize() {
            std::vector<uint8_t> serialized_packet;
            switch (this->mVersion) {
                case 2: {
                    DataPingRequestV2 packet = DataPingRequestV2();
                    packet.mType = this->mType;
                    packet.mSeq = this->mSeq;
                    _memcpy_s(packet.mIdent, LATENCYGG_IDENT_LEN, this->mIdent.c_str(), LATENCYGG_IDENT_LEN - 1);
                    packet.mTimestamp = this->mTimestamp;
                    serialized_packet = packet.serialize();
                    break;
                }
                default:
                case 3: {
                    DataPingRequestV3 packet = DataPingRequestV3();
                    packet.mType = this->mType;
                    packet.mSeq = this->mSeq;
                    _memcpy_s(packet.mToken, LATENCYGG_TOKEN_LEN, this->mToken.c_str(), LATENCYGG_TOKEN_LEN - 1);
                    packet.mTimestamp = this->mTimestamp;
                    serialized_packet = packet.serialize();
                    break;
                }
            }
            return serialized_packet;
        }




    }
}