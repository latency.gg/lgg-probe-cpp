//
// Created by tim on 15/08/2021.
//

#ifndef CPPPROBE_COMMON_HPP
#define CPPPROBE_COMMON_HPP

namespace latencygg {

    void _memcpy_s(void *dest, size_t destsz, const void *src, size_t count);

}

#endif // CPPPROBE_COMMON_HPP
