//
// Created by plain on 10/09/2021.
//
#include <stdexcept>
#include <cstring>
#include "latencygg/common.hpp"

namespace latencygg {

    void _memcpy_s(void *dest, size_t destsz, const void *src, size_t count) {
#if defined(_MSC_VER)
        auto err = memcpy_s(dest, destsz, src, count);
        if (err != 0) {
            throw std::out_of_range("attempted to write out of bounds"); // Should never happen, but best be safe
        }
#else
        memcpy(dest, src, count);
#endif
    }
}