//
// Created by plain on 09/12/2021.
//

#include <boost/format.hpp>

#include "main.h"

using namespace std;

int load_metrics(latencygg::LatencyGG<latencygg::DataPing> &latency, std::map<std::string,std::pair<std::string, std::string>> *lookupIP, nlohmann::json &stale_metrics, int ip_version, std::string &ident, std::string &source_addr) {
    int inserted = 0;
    for (const auto& [provider_name, locations] : stale_metrics.items()) {
        for (const auto& [location_name, location] : locations.items()) {
            for (const auto& beacon: location["beacons"]) {
                std::string version_str = (boost::format("ipv%1%") % ip_version).str();
                std::string target_addr = beacon[version_str];
                std::string token;
                int dpv = 3;
                if (beacon.contains("token")) {
                    token = beacon["token"];
                } else {
                    dpv = 2;
                }
                latencygg::DataPing dp(ident, source_addr, target_addr, token, dpv);
                latency.add(dp);
                (*lookupIP)[target_addr] = std::pair<std::string, std::string>(provider_name, location_name);
                inserted++;
            }
        }
    }
    return inserted;
}

int main()
{
    nlohmann::json jdata = get_metrics();
    auto source = jdata["source"];
    std::string ident = source["ident"];
    std::string source_addr = source["addr"];
    int ip_version = source["version"];
    latencygg::af_t af;
    switch (ip_version) {
        default:
        case 4:
            af = latencygg::eINET;
            break;
        case 6:
            af = latencygg::eINET6;
            break;
    }
    std::map<std::string,std::pair<std::string, std::string>> lookupIP;
    latencygg::LatencyGG<latencygg::DataPing> latency(af, 2000);
    load_metrics(latency, &lookupIP, jdata["stale_metrics"], ip_version, ident, source_addr);
    latency.run();
    auto final_metrics = jdata["clean_metrics"];
    for (auto&[target, ping] : latency) {
        auto &[provider_name, location_name] = lookupIP[target];
        auto stats = ping.getStats();
        if (final_metrics[provider_name].contains(location_name) && final_metrics[provider_name][location_name].contains("rtt")) {
            uint32_t rtt = final_metrics[provider_name][location_name]["rtt"];
            uint32_t stddev = final_metrics[provider_name][location_name]["stddev"];
            final_metrics[provider_name][location_name]["rtt"] = (rtt + stats->mRtt) / 2;
            final_metrics[provider_name][location_name]["stddev"] = (stddev + stats->mStddev) / 2;
        } else {
            final_metrics[provider_name][location_name]["rtt"] = stats->mRtt;
            final_metrics[provider_name][location_name]["stddev"] = stats->mStddev;
        }
    }
    for (const auto& [provider_name, locations] : final_metrics.items()) {
        if (locations.empty())
            continue;
        std::cout << provider_name << std::endl;
        for (const auto& [location_name, location] : locations.items()) {
            std::cout << (boost::format("\t %1%: %2% (~%3%)") %location_name %location["rtt"] %location["stddev"]).str().c_str() <<std::endl;
        }
    }
    return 0;
}