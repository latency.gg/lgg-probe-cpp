//
// Created by plain on 10/12/2021.
//

#include "curl_http.h"

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

nlohmann::json get_metrics() {
    CURL *curl;
    CURLcode res;
    std::string readBuffer;
    nlohmann::json j;

    curl = curl_easy_init();
    if(curl) {
        curl_easy_setopt(curl, CURLOPT_URL, "https://demo.latency.gg/metrics");
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
        j = nlohmann::json::parse(readBuffer);
    }
    return j;
}