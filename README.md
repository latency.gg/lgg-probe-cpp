# LatencyGG Probe Library for C++
![Coverity Scan Build Status](https://scan.coverity.com/projects/23595/badge.svg "Coverity Scan Build Status")

A header-only library for latency measurements using the LatencyGG network.

## Usage
See the example for recommended usage.

### QuickStart
First create an instance of LatencyGG specifying the type of `AF` (`eINET` for IPv4 `eINET6` for IPV6) and the `timeout` limit for pings.
```c++
latencygg::LatencyGG<latencygg::DataPing> latency(latencygg::eINET, 1000);
```
Then create instances of the Ping type _(NOTE: you cannot mix and match pings in a single run)_ that you want to perform, specifying the `target_ip` (beacon address), `own_ip` (user public ip) and `ident`(as provided by the Latency.gg Client API) 
```c++
latencygg::DataPing ping1(target_ip, own_ip, ident);
```
Next, `add` all of the pings you wish to run simultaneously (for accurate measurements don't run more than 10 measurements simultaneously). 
```c++
latency.add(ping1);
```
Finally, `run` the measurement.
```c++
latency.run();
```
If for some reason you need wish to gracefully stop a running measurement before it is complete, as you wish to retain any completed results then you can call `stop()`.
```c++
latency.stop();
```
If you need to terminate a running measurement before it is complete, discarding the measurements call `kill()`.
```c++
latency.kill();
```
To check if all measurements are complete you can poll the `isComplete()` method.
```c++
if (latency.isComplete()) {
    // Do something
}
```
To retrieve results you can iterate over the pings using the STL-compatible `begin()` and `end()` iterators, or by requesting measurements by the `target_ip` via the map-like interface.
```c++
// Range for loop
for (auto&[target, ping] : latency) {
    std::cout << "Target: " << target << " -> " << ping.asJson() << std::endl;
}
// Map-like interface
latencygg::DataPing ping = latency[target_ip];
std::cout << "Target: " << target_ip << " -> " << ping.asJson() << std::endl;
```



## Compiling the example
```bash
mkdir build
cd build/
cmake ../
cmake --build .
```

## Running the example
```bash
./example
```

## Style guide
Generally speaking, comply to [Mozilla-style](https://firefox-source-docs.mozilla.org/code-quality/coding-style/coding_style_cpp.html).
However, ignore Mozilla's UpperCamelCase method name recommendation as this breaks STL compatability (e.g. `begin()` and `end()` as needed for range based for loops)
